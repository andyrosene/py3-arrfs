import unittest
from arrfs.alink import ALink
from arrfs.__constants__ import d_home


class TestALink(unittest.TestCase):
    test_dir = d_home.child_dir('unittest_dir')
    test_link = test_dir.child_link('test-link')

    def test_init(self):
        pass

    def test_qpath(self):
        self.assertEqual(self.test_link.qpath, '"{}"'.format(self.test_link.path))


if __name__ == '__main__':
    unittest.main()
