import unittest
from arrfs.afile import AFile
from arrfs.__constants__ import d_home


class TestAFile(unittest.TestCase):
    test_dir = d_home.child_dir('unittest_dir')
    test_file = test_dir.child_file('test-file.txt')

    def test_init(self):
        pass

    def test_qpath(self):
        self.assertEqual(self.test_file.qpath, '"{}"'.format(self.test_file.path))


if __name__ == '__main__':
    unittest.main()
