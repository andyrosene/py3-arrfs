import unittest

from arrfs.pcache import PCache
from os import getenv


class TestPCache(unittest.TestCase):
    def test_can_read(self):
        self.assertTrue(PCache.can_read('/etc'))
        self.assertTrue(PCache.can_read(getenv('HOME')))
        self.assertFalse(PCache.can_read('/root'))

    def test_can_write(self):
        self.assertFalse(PCache.can_write('/etc'))
        self.assertTrue(PCache.can_write(getenv('HOME')))
        self.assertFalse(PCache.can_write('/root'))

    def test_can_execute(self):
        self.assertTrue(PCache.can_execute('/etc'))
        self.assertTrue(PCache.can_execute(getenv('HOME')))
        self.assertFalse(PCache.can_execute('/root'))

    def test_get_all_available_permissions(self):
        r, w, x = PCache.get_all_available_permissions('/etc')
        self.assertTrue(r)
        self.assertFalse(w)
        self.assertTrue(x)

        r, w, x = PCache.get_all_available_permissions(getenv('HOME'))
        self.assertTrue(r)
        self.assertTrue(w)
        self.assertTrue(x)

        r, w, x = PCache.get_all_available_permissions('/root')
        self.assertFalse(r)
        self.assertFalse(w)
        self.assertFalse(x)

    def test_invalidate(self):
        # Load something into the cache if it is empty for some reason.
        if len(PCache.__cache__.keys()) == 0:
            PCache.can_read('/etc')

        # Verify it is not empty.
        self.assertNotEqual(len(PCache.__cache__.keys()), 0, "Expected the PCache to not be empty")
        
        # Remove all of the items (this will clear the entire cache)
        PCache.invalidate('', True)

        # Verify it is now empty
        self.assertEqual(len(PCache.__cache__.keys()), 0, "Expected the PCache to be empty")


if __name__ == '__main__':
    unittest.main()
