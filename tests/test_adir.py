import unittest
from arrfs.adir import ADir
from arrfs.__constants__ import d_home


class TestADir(unittest.TestCase):
    test_dir = d_home.child_dir('unittest_dir')

    def test_init(self):
        pass

    def test_qpath(self):
        self.assertEqual(self.test_dir.qpath, '"{}"'.format(self.test_dir.path))


if __name__ == '__main__':
    unittest.main()
