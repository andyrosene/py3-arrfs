# py3-arrfs

This module provides some useful high level file system classes similar to those from Java. This is a limited amount of
functionality; but, does simplify a number of operations.

## Requirements

* `arrelevator` -- This is necessary for file operations that require elevated permissions to perform.
