from arruser import belongs_to_group, get_username
from os.path import exists
from subprocess import Popen, PIPE

__all__ = [
    'PCache',
]


class PCache(object):
    __cache__ = {}

    @staticmethod
    def __available_permissions(path: str) -> (bool, bool, bool):
        if not exists(path):
            return True, True, True
        
        if path in PCache.__cache__:
            return PCache.__cache__[path]
        
        p1 = Popen(['ls', '-ld', path], stdout=PIPE, stderr=PIPE, universal_newlines=True)
        stdout, stderr = p1.communicate()
        if stderr.strip() != '':
            raise AssertionError(stderr)

        line = stdout.strip()
        if line == '' or len(line.splitlines()) != 1:
            raise AssertionError('no permissions were acquired')

        line_parts = line.split()

        permission_settings = line_parts[0]
        owner_perms = permission_settings[1:4]
        group_perms = permission_settings[4:7]
        all_perms = permission_settings[7:10]

        owner = line_parts[2]
        group = line_parts[3]

        # Whether group and owner perms are allowed
        allow_group_perms = belongs_to_group(group)
        allow_owner_perms = get_username() == owner

        def __has_read_perm(perms: str) -> bool:
            return perms[0] == 'r'

        def __has_write_perm(perms: str) -> bool:
            return perms[1] == 'w'

        def __has_execute_perm(perms: str) -> bool:
            return perms[2] == 'x'

        # Set the read, write, and execute permissions
        allow_read = __has_read_perm(all_perms) or \
                     (allow_group_perms and __has_read_perm(group_perms)) or \
                     (allow_owner_perms and __has_read_perm(owner_perms))
        allow_write = __has_write_perm(all_perms) or \
                      (allow_group_perms and __has_write_perm(group_perms)) or \
                      (allow_owner_perms and __has_write_perm(owner_perms))
        allow_exec = __has_execute_perm(all_perms) or \
                     (allow_group_perms and __has_execute_perm(group_perms)) or \
                     (allow_owner_perms and __has_execute_perm(owner_perms))

        # Add to the cache
        PCache.__cache__[path] = allow_read, allow_write, allow_exec

        # Return the values
        return allow_read, allow_write, allow_exec

    @staticmethod
    def can_write(path: str) -> bool:
        _, w, _ = PCache.__available_permissions(path)
        return w

    @staticmethod
    def can_read(path: str) -> bool:
        r, _, _ = PCache.__available_permissions(path)
        return r

    @staticmethod
    def can_execute(path: str) -> bool:
        _, _, x = PCache.__available_permissions(path)
        return x

    @staticmethod
    def get_all_available_permissions(path: str) -> (bool, bool, bool):
        return PCache.__available_permissions(path)

    @staticmethod
    def invalidate(path: str, is_dir=False):
        # delete 1 key only if not a directory
        if not is_dir:
            # delete the single key by it's path
            if path in PCache.__cache__:
                del PCache.__cache__[path]

            # Return either way
            return

        # If a directory was given, delete the directory key and all affected children keys.
        f_path = path + '/'
        curr_keys = PCache.__cache__.copy().keys()  # iterate over the shallow copy
        for k in curr_keys:
            if k == path or k.startswith(f_path):
                del PCache.__cache__[k]
