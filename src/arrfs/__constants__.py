from os import getenv
from .adir import ADir
from .afile import AFile

__all__ = [
    'd_pictures',
    'd_staging',
    'f_dot_profile',
    'd_usr_local',
    'd_usr',
    'd_dot_local_share',
    'd_dot_local_old_share',
    'f_local_fonts_scale',
    'd_workspace_personal',
    'd_dot_config_autostart',
    'd_dot_local_share_applications',
    'd_dot_local_old_share_applications',
    'd_usr_share_applications',
    'd_dot_local_old',
    'f_local_fonts_dir',
    'd_workspace_jht',
    'd_network_mounts',
    'd_usr_local_bin',
    'd_dot_config_old',
    'd_downloads',
    'd_documents',
    'd_desktop',
    'd_dot_gnome',
    'f_dot_bashrc',
    'd_etc_apt',
    'd_etc',
    'd_usr_share',
    'd_snap',
    'd_usr_bin',
    'd_dot_local',
    'd_dot_cache',
    'd_dot_config',
    'd_home',
    'd_dot_fonts',
    'd_dot_themes',
    'd_dot_icons',
    'd_workspace',
]

d_network_mounts: ADir = ADir('/media/sharedrives')
"""
The parent directory for all of the network mount locations. This is the same regardless of the usage scenario.
"""

d_home: ADir = ADir(getenv('HOME'))
"""
The ~/ directory.
"""

d_dot_cache: ADir = d_home.child_dir('.cache')
"""
The ~/.cache directory.
"""

d_dot_config: ADir = d_home.child_dir('.config')
"""
The ~/.config directory
"""

d_dot_config_old: ADir = d_home.child_dir('.config_old')
"""
The ~/.config_old directory
"""

d_dot_config_autostart: ADir = d_dot_config.child_dir('autostart')
"""
~/.config/autostart - The directory containing all of the autostart icons.
"""

d_dot_fonts: ADir = d_home.child_dir('.fonts')
"""
~/.fonts - The directory fonts are installed in locally.
"""

d_dot_gnome: ADir = d_home.child_dir('.gnome')
"""
The ~/.gnome directory. This is, as far as I can tell, deprecated.
"""

d_dot_icons: ADir = d_home.child_dir('.icons')
"""
~/.icons - The directory icons are installed in locally.
"""

d_dot_local: ADir = d_home.child_dir('.local')
"""
The ~/.local directory.
"""

d_dot_local_old: ADir = d_home.child_dir('.local_old')
"""
The ~/.local_old directory (remove once empty)
"""

d_dot_local_old_share: ADir = d_dot_local_old.child_dir('share')
"""
The ~/.local_old/share directory
"""

d_dot_local_old_share_applications: ADir = d_dot_local_old_share.child_dir('applications')
"""
The OLD directory containing the local app icons. This will be deprecated once fully removed.
"""

d_dot_local_share: ADir = d_dot_local.child_dir('share')
"""
The ~/.local/share directory
"""

d_dot_local_share_applications: ADir = d_dot_local_share.child_dir('applications')
"""
The directory containing the local application icons.
"""

d_dot_themes: ADir = d_home.child_dir('.themes')
"""
~/.themes - the local themes installation directory
"""

d_desktop: ADir = d_home.child_dir('Desktop')

d_documents: ADir = d_home.child_dir('Documents')

d_snap: ADir = d_home.child_dir('snap')
"""
The ~/snap directory. This contains the remnants from a snap installation.
"""

d_staging: ADir = d_home.child_dir('staging')
"""
A temp dir that I use for staging changes that needs to be cleaned up (e.g. downloading and extracting).
"""

d_downloads: ADir = d_home.child_dir('Downloads')

d_pictures: ADir = d_home.child_dir('Pictures')
"""
The ~/Pictures directory.
"""

d_workspace: ADir = d_home.child_dir('Workspace')

d_workspace_personal: ADir = d_workspace.child_dir('arr')

d_workspace_jht: ADir = d_workspace.child_dir('t_jht')

d_usr: ADir = ADir('/usr')
"""
The /usr directory
"""

d_usr_bin: ADir = d_usr.child_dir('bin')
"""
The /usr/bin directory (global scripts directory).
"""

d_usr_share: ADir = d_usr.child_dir('share')
"""
/usr/share (global alternative to ~/.local/share)
"""

d_usr_share_applications: ADir = d_usr_share.child_dir('applications')
"""
/usr/share/applications (global application icons directory)
"""

d_usr_local: ADir = d_usr.child_dir('local')
"""
/usr/local
"""

d_usr_local_bin: ADir = d_usr.child_dir('bin')
"""
/usr/local/bin
"""

d_etc: ADir = ADir('/etc/')
"""
/etc directory
"""

d_etc_apt: ADir = d_etc.child_dir('apt')
"""
/etc/apt directory
"""

'''
------------------------------------------------------------------------------------------------------------------------
All of the commonly used files here
------------------------------------------------------------------------------------------------------------------------
'''

f_dot_bashrc: AFile = d_home.child_file('.bashrc')
"""
The ~/.bashrc file
"""

f_dot_profile: AFile = d_home.child_file('.profile')
"""
The ~/.profile file
"""

f_local_fonts_scale: AFile = d_dot_fonts.child_file('fonts.scale')
"""
the fonts.scale file in ~/.fonts created by mkfontscale
"""

f_local_fonts_dir: AFile = d_dot_fonts.child_file('fonts.dir')
"""
the fonts.dir file in ~/.fonts created by mkfontdir
"""
