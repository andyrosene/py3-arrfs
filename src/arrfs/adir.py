from arrelevator import *
from os import chdir, listdir, system
from os.path import basename, dirname, exists, isdir, isfile, islink, join
from subprocess import run
from typing import List
from typing import TYPE_CHECKING
if TYPE_CHECKING:
    from .afile import AFile
    from .alink import ALink

__all__ = [
    'ADir',
]


class ADir(object):
    def __init__(self, path: str):
        if dirname(path) == '':
            raise AssertionError('The given path is invalid: \"' + path + '\"')

        self.path = path
        self.name = basename(path)
        self.parent = None if self.path == '/' else ADir(dirname(self.path))

    def __str__(self) -> str:
        return self.path

    def __eq__(self, other):
        return isinstance(other, self.__class__) and self.path == other.path

    def __ne__(self, other):
        return not self.__eq__(other)

    @property
    def qpath(self) -> str:
        """The path enclosed in quotes. I don't think this is used anywhere anymore.

        Returns:
            str: The path enclosed in quotes.
        """
        return '"{}"'.format(self.path)

    def is_empty(self) -> bool:
        """Check if this directory is empty or not.

        Returns:
            bool: True if empty, else False.
        """
        return len(self.listdir()) == 0

    def listdir(self) -> List[str]:
        return [] if not self.exists() else listdir(self.path)

    def get_children(self) -> (List['AFile'], List['ALink'], List['ADir']):
        from .afile import AFile
        from .alink import ALink

        files: List[AFile] = []
        links: List[ALink] = []
        dirs: List[ADir] = []

        for fname in self.listdir():
            child_path = join(self.path, fname)

            if islink(child_path):
                links.append(ALink(child_path))
            elif isfile(child_path):
                files.append(AFile(child_path))
            elif isdir(child_path):
                dirs.append(ADir(child_path))
            # Ignore any other child types

        return files, links, dirs

    def get_child_files(self) -> List['AFile']:
        files, _, _ = self.get_children()
        return files

    def get_child_links(self) -> List['ALink']:
        _, links, _ = self.get_children()
        return links

    def get_child_dirs(self) -> List['ADir']:
        _, _, dirs = self.get_children()
        return dirs

    def find_child_files_by_name_noext(self, name_noext: str) -> List['AFile']:
        """Find and return all child files with the given name minus extension.

        Args:
            name_noext: the name of the file without the extension.

        Returns:
            List['AFile']: the list of all files with that name minus the extension. If there are none, the list will be
            empty.
        """
        result = []

        for child_file in self.get_child_files():
            if child_file.name_noext == name_noext:
                result.append(child_file)

        return result

    def child_dir(self, name: str) -> 'ADir':
        return ADir(join(self.path, name))

    def child_file(self, name: str) -> 'AFile':
        from .afile import AFile
        return AFile(join(self.path, name))

    def child_link(self, name: str) -> 'ALink':
        from .alink import ALink
        return ALink(join(self.path, name))

    def exists(self, required: bool = False) -> bool:
        """Check whether this directory exists or not.

        Args:
            required: whether or not existence is required.

        Returns:
            bool: True if this directory exists, else False.

        Raises:
            AssertionError: This directory did not exist and it's existence was required.
        """
        if exists(self.path):
            if not isdir(self.path):
                raise AssertionError(self.path + ' exists but is not a directory.')

            return True

        if not required:
            return False

        raise AssertionError('There is no directory at: ' + self.path)

    def create(self) -> bool:
        from .pcache import PCache

        if self.exists():
            return False

        if self.parent is not None:
            self.parent.create()

        permitted = PCache.can_write(self.path) and (self.parent is None or PCache.can_write(self.parent.path))
        if not permitted:
            elevate('Creating ' + self.path)

        # fixme - this should be a run so I can do something with the failures.
        result = system('{}mkdir "{}"'.format(
            'sudo ' if not permitted else '',
            self.path
        ))
        if result != 0:
            raise AssertionError('Failed to create ' + self.path + ' directory.\nResult: ' + str(result))

        return True

    def delete(self, silent=False):
        from .pcache import PCache

        if not self.exists():
            return

        if self.parent is None:
            raise AssertionError('Cannot delete the root directory')

        # First attempt at lower permissions, then elevate and retry if this failed (the proper way would be to check
        # all child permissions too).
        result = system('rm -rf "{}"'.format(self.path))
        if result != 0:
            elevate('Deleting ' + self.path if not silent else '')
            result = system('sudo rm -rf "{}"'.format(self.path))
            if result != 0:
                raise AssertionError('failed to delete ' + self.path + '.\nResult: ' + str(result))

        PCache.invalidate(self.path, is_dir=True)

    def move(self, destination: 'ADir', overwrite: bool = False) -> 'ADir':
        from .pcache import PCache

        if self.parent is None:
            raise AssertionError('Moving root directory is not allowed')

        if destination.parent is None:
            raise AssertionError('Cannot target the root directory')

        if destination.path == self.path:
            print('already occupying', destination)
            return destination

        if not overwrite and destination.exists():
            print(destination, 'directory already exists')
            return destination

        if not self.exists():
            raise AssertionError('No directory at: ' + self.path)

        destination.parent.create()
        destination.delete()

        permitted = PCache.can_write(self.path) and PCache.can_write(self.parent.path) and PCache.can_write(destination.parent.path)
        if not permitted:
            elevate('Moving ' + self.path + ' to ' + destination.path)

        # fixme - this is likely going to fail at somepoint because of child permissions. This needs to be a subprocess
        #  to properly handle the actual error
        result = system('{}mv "{}" "{}"'.format(
            'sudo ' if not permitted else '',
            self.path,
            destination.path
        ))
        if result != 0:
            raise AssertionError('Error moving {} to {}.\nResult: {}'.format(
                self.path,
                destination.path,
                result
            ))

        PCache.invalidate(self.path, is_dir=True)
        return destination

    def rename(self, name: str, overwrite: bool = False) -> 'ADir':
        if self.parent is None:
            raise AssertionError('Cannot rename the root directory')

        if name is None or len(name.strip()) == 0:
            raise AssertionError('must provide a non-empty name')

        return self.move(self.parent.child_dir(name), overwrite)

    def change_into(self):
        if not self.exists():
            raise AssertionError(self.path + ' does not exist, illegal cd')

        chdir(self.path)

    def copy(self, destination: 'ADir', overwrite: bool = False):
        from .pcache import PCache

        if destination.parent is None:
            raise AssertionError('Targeting the root directory is prohibited')

        if destination.path == self.path:
            print('Contents already present at', destination.path)
            return

        if not overwrite and destination.exists():
            print(destination.path, 'directory already exists')
            return

        if not self.exists():
            raise AssertionError('No directory at: ' + self.path)

        destination.parent.create()
        destination.delete()

        # fixme - this should instead be leveraging popen and elevating on failure.

        permitted = PCache.can_write(self.path) and \
            (self.parent is None or PCache.can_write(self.parent.path)) and \
            (PCache.can_write(destination.parent.path))
        if not permitted:
            elevate('Copying ' + self.path + ' to ' + destination.path)

        result = system('{}cp -rf "{}" "{}"'.format(
            'sudo ' if not permitted else '',
            self.path,
            destination.path
        ))
        if result != 0:
            raise AssertionError('Error copying {} to {}.\nResult: {}'.format(
                self.path,
                destination.path,
                result
            ))

    def empty(self, require_sync: bool = False):
        """Remove all contents from this directory.

        Notes:
            - This always will be performed from an elevated execution environment.

        Args:
            require_sync: whether or not the sync command must be run following this command (regardless of outcome)
        """
        elevate('Emptying ' + self.path)
        cmd = 'sudo rm -rf {}/*'.format(self.path)
        result = system(cmd)

        if require_sync:
            print('Synchronizing changes')
            system('sync')

        if result != 0:
            raise AssertionError('Failed to empty ' + self.path + ' using cmd:\n\n\t' + cmd)

    def copy_contents(self, destination: 'ADir', require_sync: bool = False):
        """Copy the contents of this directory to the given directory **as is**. This is accomplished using the ``cp``
        command with option ``-a`` which does the following:

        +---------------------------------+-----------------------------------------------+
        | Option                          | Description                                   |
        +=================================+===============================================+
        | ``-a``, ``--archive``           | same as ``-dR --preserve=all``                |
        +---------------------------------+-----------------------------------------------+
        | ``-d``                          | same as ``--no-dereference --preserve=links`` |
        +---------------------------------+-----------------------------------------------+
        | ``-P``, ``--no-dereference``    | never follow symbolic links in SOURCE         |
        +---------------------------------+-----------------------------------------------+
        | ``--preserve[=ATTR_LIST]``      | preserve the specified attributes (default:   |
        |                                 | mode,ownership,timestamps), if possible       |
        |                                 | additional attributes: context, links, xattr, |
        |                                 | all                                           |
        +---------------------------------+-----------------------------------------------+
        | ``-R``, ``-r``, ``--recursive`` | copy directories recursively                  |
        +---------------------------------+-----------------------------------------------+

        These options will likely be configurable in the future. However, for the time being this is the only option.

        Args:
            destination: The directory to copy the contents of this directory to.

            require_sync: Whether or not the sync command must run (regardless of this copy occur -- provided a copy is
            initiated).

        Raises:
            AssertionError: If either this directory or the destination directory does not exist or the destination
            directory is not empty.

            CalledProcessError: The copy command failed.
        """
        # Ensure the directories actually exist
        self.exists(required=True)
        destination.exists(required=True)

        if not destination.is_empty():
            raise AssertionError('Attempted to copy contents of {src} into {dest} but {dest} is not empty.'.format(
                src=self.path, dest=destination.path))

        elevate('++ Copying over the needed files')
        r1 = run('sudo cp -a * "{}"'.format(destination.path), shell=True, cwd=self.path, capture_output=True)

        if require_sync:
            run(['sync'])

        r1.check_returncode()  # Run the delayed return code check

    def has_git_changes(self) -> bool:
        """Check whether this directory has git changes. This will result in a call to ``change_into`` which requires
        this directory exists. Important to remember the directory must exist and you will be changing into it.

        Returns:
            bool: True if this directory has git changes, else False.

        Raises:
            AssertionError: This was called on a directory that does not exist.
        """
        self.change_into()
        return system('git status | grep "nothing to commit, working tree clean"') != 0
