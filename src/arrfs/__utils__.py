from os.path import dirname, realpath
from .adir import ADir

__all__ = [
    'get_real_parent',
]


def get_real_parent(file_path: str) -> ADir:
    """Get the real parent directory for the file at the given path.

    This was created with the idea that it would be paired with ``__file__`` to find the actual directory for the given
    file (regardless of symbolic linking).

    Notes:
        - This is not a re-implementation of ``AFile(file_path).parent``. That assumes the absolute path was already
        provided.

    Args:
        file_path: The path to the file whose parent is desired.

    Returns:
        ADir: The real parent directory for the given file path.
    """
    return ADir(dirname(realpath(file_path)))
