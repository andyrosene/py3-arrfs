from arrelevator import *
from datetime import datetime
from os import system
from os.path import basename, dirname, exists, isfile, splitext
from subprocess import PIPE, Popen
from typing import TYPE_CHECKING
if TYPE_CHECKING:
    from .adir import ADir

__all__ = [
    'AFile',
]


class AFile(object):
    @staticmethod
    def isfile(path: str) -> bool:
        """Check if the given path is referencing a file or not. This was added for convenience.

        Args:
            path: The **absolute** path being checked.

        Returns:
            bool: True if the given path is for a file, else False.
        """
        return isfile(path)

    def __init__(self, path: str):
        from .adir import ADir

        if dirname(path) == '':
            raise AssertionError('The given path is invalid: \"' + path + '\"')

        self.path = path
        self.name = basename(self.path)
        # Set the name without extension, and the extension (extension is prefixed with '.' if available).
        self.name_noext, self.ext = splitext(self.name)
        self.parent = ADir(dirname(path))

    def __eq__(self, other):
        return isinstance(other, self.__class__) and self.path == other.path

    def __ne__(self, other):
        return not self.__eq__(other)

    def __str__(self):
        return self.path

    @property
    def qpath(self) -> str:
        """Get the path enclosed in double quotes. Commonly needed for script execution.

        Returns:
            str: The path enclosed in double quotes.
        """
        return '"{}"'.format(self.path)

    def exists(self, required: bool = False) -> bool:
        """Check whether this file exists or not.

        Args:
            required: Whether or not existence is required.

        Returns:
            bool: True if this file exists, else False.

        Raises:
            AssertionError: This file did not exist and it's existence was required.
        """
        if exists(self.path):
            if not isfile(self.path):
                raise AssertionError(self.path + ' exists but is not a file.')

            return True

        if not required:
            return False

        raise AssertionError('There is no file at: ' + self.path)

    def has_extension(self, ext: str, required: bool = False) -> bool:
        """Check whether this file has the given extension or not.

        Warnings:
            1. The given extension **must** begin with a '.' to match, if there is indeed an extension.

        Args:
            ext: The desired extension for this file (**must** start with '.').

            required: Whether or not this extension is required.

        Returns:
            bool: True if the file has the given extension, else False.

        Raises:
            AssertionError: This files extension does not match the given extension and required is True.
        """
        if self.ext.lower() == ext.lower():
            return True

        if not required:
            return False

        raise AssertionError(self.path + ' does not have file extension (' + ext + ') it is (' + self.ext + ')')

    def create(self, content: str = None, overwrite: bool = False) -> bool:
        from .pcache import PCache
        from .__constants__ import d_staging

        if self.exists():
            if not overwrite:
                print(self.path, 'already exists')
                return False

            self.delete()  # delete the existing file to avoid issues later
        else:
            self.parent.create()  # create the parent if not existing

        permitted = PCache.can_write(self.path) and PCache.can_write(self.parent.path)

        if content is None:
            if not permitted:
                elevate('Creating ' + self.path)

            result = system('{}touch "{}"'.format('sudo ' if not permitted else '', self.path))
            if result != 0:
                raise AssertionError('Error creating {}.\nResult: {}'.format(self.path, result))

            return True

        result = False  # initialize as not created.

        try:
            d_staging.create()
            staged_file = d_staging.child_file(basename(self.path))
            print('Writing contents to a staged file')
            f = open(staged_file.path, 'w')
            f.write(content)
            f.close()

            staged_file.move(self)
            result = True  # Mark this as having actually been created

        finally:
            d_staging.delete()
            return result  # return whether it was fully created or not.

    def move(self, d_file: 'AFile', overwrite: bool = False) -> 'AFile':
        from .pcache import PCache

        if d_file.path == self.path:
            print(d_file, 'file already exists')
            return d_file

        if not overwrite and d_file.exists():
            print(d_file, 'file already exists.')
            return d_file

        if not self.exists():
            raise AssertionError('No file at ' + self.path)

        d_file.parent.create()
        d_file.delete()

        permitted = PCache.can_write(self.path) and PCache.can_write(self.parent.path) and \
            PCache.can_write(d_file.parent.path)

        if not permitted:
            elevate('Moving ' + self.path + ' to ' + d_file.path)

        result = system('{}mv "{}" "{}"'.format(
            'sudo ' if not permitted else '',
            self.path,
            d_file.path
        ))
        if result != 0:
            raise AssertionError('Error moving {} to {}.\nResult: {}'.format(self.path, d_file.path, result))

        # Invalidate the permission cache for this item
        PCache.invalidate(self.path)

        return d_file

    def delete(self, silent: bool = False) -> bool:
        """Delete this file if it exists.

        Args:
            silent: Whether or not to suppress all messages related to the deletion of this file.

        Returns:
            bool: Whether or not this file actually was deleted by this function.

        Raises:
            AssertionError: Something unexpected went wrong when the delete operation was performed.
        """
        from .pcache import PCache

        if not self.exists():
            return False

        permitted = PCache.can_write(self.path) and PCache.can_write(self.parent.path)

        if not permitted:
            elevate('Deleting ' + self.path if not silent else '')  # damn. I need to be able to support the silent version

        result = system('{}rm -f "{}"'.format(
            'sudo ' if not permitted else '',
            self.path
        ))
        if result != 0:
            raise AssertionError('failed to delete {}.\nResult: {}'.format(self.path, result))

        # Invalidate the permission cache
        PCache.invalidate(self.path)

        return True

    def rename(self, name: str, overwrite: bool = False) -> 'AFile':
        if name is None or len(name.strip()) == 0:
            raise AssertionError('must provide a non-empty name')

        return self.move(self.parent.child_file(name), overwrite)

    def copy(self, d_file: 'AFile', overwrite: bool = False) -> bool:
        """ Copy this file to the absolute path of the given destination file. This will automatically elevate
        permissions if necessary. If the given destination file already exists, this operation will be ignored unless
        overwrite is True. If overwrite is True, the destination file will be deleted prior to moving this file over.

        Args:
            d_file: The destination this file is being copied to.

            overwrite: Whether or not to overwrite an existing destination file.

        Returns:
            bool: True if the copy was performed, else False.

        Raises:
            AssertionError: This is raised if (1) this source file does not exist or (2) something unexpected went wrong
            while performing the copy operation.
        """
        from .pcache import PCache

        if d_file.path == self.path:
            print('{} file already exists'.format(self.path))
            return False

        if not overwrite and d_file.exists():
            print(d_file.path, 'file already exists')
            return False

        if not self.exists():
            raise AssertionError('no file at: ' + self.path)

        d_file.parent.create()
        d_file.delete()

        # This does not require you have write permissions on the file or parent but may actually require you have read
        # permissions. I will need to test this at some point.
        permitted = PCache.can_write(d_file.parent.path)

        if not permitted:
            elevate('Copying ' + self.path + ' to ' + d_file.path)

        result = system('{}cp -f "{}" "{}"'.format(
            'sudo ' if not permitted else '',
            self.path,
            d_file.path
        ))
        if result != 0:
            raise AssertionError('Error copying {} to {}.\nResult: {}'.format(self.path, d_file.path, result))

        return True

    def md5(self) -> str:
        """Calculate and return the MD5 for this file.

        Returns:
            str: The MD5 for this file.

        Raises:
            AssertionError: If either (1) this file does not exist or (2) something unexpected happened while
            calculating the MD5 for this file.
        """
        print('Calculating the MDF of', self.path)
        if not self.exists():
            raise AssertionError('No file at ' + self.path)

        p1 = Popen(['md5sum', self.path],
                   stdout=PIPE,
                   stderr=PIPE,
                   universal_newlines=True)
        p2 = Popen(['awk', '{print $1}'],
                   stdin=p1.stdout,
                   stdout=PIPE,
                   stderr=PIPE,
                   universal_newlines=True)
        stdout, stderr = p2.communicate()
        if stderr.strip() != '':
            raise AssertionError(stderr)

        return stdout.strip()

    def extract(self, extract_dir: 'ADir' = None, cleanup: bool = False):
        """Currently has support for ``.gz`` and ``.zip``.

        fixme - add support for more types.

        Args:
            extract_dir:
            cleanup:
        """
        self.exists(True)
        if self.has_extension('.gz'):
            self.__extract_gz(extract_dir, cleanup)
        elif self.has_extension('.zip'):
            self.__extract_zip(extract_dir, cleanup)
        else:
            raise AssertionError(self.path + ' does not have a supported archive file extension.')

    def __extract_gz(self, extract_dir: 'ADir' = None, cleanup: bool = False):
        """fixme - document

        Args:
            extract_dir:
            cleanup:
        """
        from .pcache import PCache

        try:
            dir_change_addon = ''

            # Check whether the current user has permission to extract into the desired directory. If an output
            # directory is specified, ensure the directory exists and update the dir_change_addon accordingly.
            if extract_dir is not None:
                extract_dir.create()
                has_permission = PCache.can_write(extract_dir.path)
                dir_change_addon = ' -C "{}"'.format(extract_dir)
            else:
                has_permission = PCache.can_write(self.parent.path)

            if not has_permission:
                elevate('Extracting {} into {}'.format(
                    self.path,
                    extract_dir.path if extract_dir is not None else self.parent.path
                ))

            # Generate the command (useful for testing purposes)
            command = '{}tar -xzf "{}"{}'.format(
                'sudo ' if not has_permission else '',
                self.path,
                dir_change_addon
            )
            print('> Executing:', command)

            # Execute the command
            if system(command) != 0:
                raise AssertionError('failed to extract {}'.format(self.path))

        finally:
            if cleanup:
                self.delete()

    def __extract_zip(self, extract_dir: 'ADir' = None, cleanup: bool = False):
        """fixme - document

        Args:
            extract_dir:
            cleanup:
        """
        from .pcache import PCache

        try:
            dir_change_addon = ''

            # Check whether the current user has permission to extract into the desired directory. If an output
            # directory is specified, ensure the directory exists and update the dir_change_addon accordingly.
            if extract_dir is not None:
                extract_dir.create()
                has_permission = PCache.can_write(extract_dir.path)
                dir_change_addon = '-d "{}" '.format(extract_dir)
            else:
                has_permission = PCache.can_write(self.parent.path)

            if not has_permission:
                elevate('Extracting {} into {}'.format(
                    self.path,
                    extract_dir.path if extract_dir is not None else self.parent.path
                ))

            # Generate the command (useful for testing purposes)
            command = '{}unzip {}"{}"'.format(
                'sudo ' if not has_permission else '',
                dir_change_addon,
                self.path
            )
            print('> Executing:', command)

            # Execute the command
            if system(command) != 0:
                raise AssertionError('failed to extract {}'.format(self.path))

        finally:
            if cleanup:
                self.delete()

    def get_last_modified_datetime(self) -> datetime:
        """Return the last modification datetime for this file. If this file does not exist, an AssertionError is
        raised.

        Returns:
            datetime: The last time this file was modified.
        """
        if not self.exists():
            raise AssertionError('Cannot acquire the last modification from a non-existent file: ' + self.path)

        p1 = Popen(['stat', '-c', '%y', self.path], stdout=PIPE, stderr=PIPE, universal_newlines=True)
        p2 = Popen(['awk', '{print $1}'], stdin=p1.stdout, stdout=PIPE, stderr=PIPE, universal_newlines=True)
        stdout, stderr = p2.communicate()
        if stderr.strip() != '':
            raise AssertionError(stderr)

        return datetime.strptime(stdout.strip(), '%Y-%m-%d')

    def set_last_modified_datetime(self, lmt: datetime):
        """Set the last modified datetime for this file using the given last modified time.

        Args:
            lmt: The last modified time as a datetime.
        """
        if not self.exists():
            raise AssertionError('Cannot set the last modified time for a non-existent file: ' + self.path)

        result = system('touch -md "{}" "{}"'.format(
            lmt.date().isoformat(),
            self.path
        ))
        if result != 0:
            raise AssertionError('Failed to set the last modified time of {} to {}.\nResult: {}'.format(
                self.path,
                lmt.date().isoformat(),
                result
            ))
