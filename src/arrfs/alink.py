from arrelevator import *
from os import system
from os.path import basename, dirname, islink, lexists, realpath

__all__ = [
    'ALink',
]


class ALink(object):
    def __init__(self, path: str):
        from .adir import ADir

        if dirname(path) == '':
            raise AssertionError('The given path is invalid: \"' + path + '\"')

        self.path = path
        self.name = basename(self.path)
        self.parent = ADir(dirname(path))

    def __str__(self):
        return self.path

    def __eq__(self, other):
        return isinstance(other, self.__class__) and self.path == other.path

    def __ne__(self, other):
        return not self.__eq__(other)

    @property
    def realpath(self) -> str:
        return realpath(self.path)

    @property
    def qpath(self) -> str:
        """Get the path enclosed in double quotes. Commonly needed for script execution.

        Returns:
            str: The path enclosed in double quotes.
        """
        return '"{}"'.format(self.path)

    def exists(self, required: bool = False) -> bool:
        """Check whether this link exists or not.

        Args:
            required: Whether or not existence is required.

        Returns:
            bool: True if this link exists, else False

        Raises:
            AssertionError: This link did not exist and it's existence was required.
        """
        if lexists(self.path):
            if not islink(self.path):
                raise AssertionError(self.path + ' exists but is not a link')

            return True

        if not required:
            return False

        raise AssertionError('There is no link at: ' + self.path)

    def create(self):
        raise AssertionError('todo(arr) - create this when needed')

    def move(self):
        raise AssertionError('todo(arr) - create this when needed')

    def delete(self) -> bool:
        from .pcache import PCache

        if not self.exists():
            return False

        permitted = PCache.can_write(self.path) and PCache.can_write(self.parent.path)

        if not permitted:
            elevate('Deleting ' + self.path)

        result = system('{}rm -f "{}"'.format(
            'sudo ' if not permitted else '',
            self.path
        ))
        if result != 0:
            raise AssertionError('failed to delete {}.\nResult: {}'.format(self.path, result))

        # Invalidate the permission cache
        PCache.invalidate(self.path)
        return True

    def rename(self):
        raise AssertionError('todo(arr) - create this when needed')

    def copy(self):
        raise AssertionError('todo(arr) - create this when needed')
